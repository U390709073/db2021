use company;

create temporary table TopSuppliers(
	SupplierID int primary key,
    SupplierNamer varchar(45),
    ProductCount int
);

insert into TopSuppliers(SupplierID, SupplierNamer, ProductCount)
select suppliers.SupplierID, SupplierName, count(ProductID) as ProductCount
from suppliers join products on suppliers.SupplierID=products.SupplierID
group by SupplierName
order by ProductCount desc;

select * from TopSuppliers
where ProductCount>3
order by ProductCount desc;
call check_table_exists("TopSuppliers");
drop temporary table TopSuppliers;

select * from customers;

update customers 
set CustomerName = "Halil İbrahim Ceylan", City = "Mugla", PostalCode="480000", Country = "TC"
where CustomerID = 1;

delete from customers where CustomerID = 1;

truncate table customers;
delete from customers;
drop table customers;
